#include "tcpServer.hpp"

#include <iostream>
#include <memory>

namespace server {
    using boost::asio::ip::tcp;
    using boost::asio::ip::address;

    TCPServer::TCPServer(const std::string& ip, int port)
        : m_acceptor(m_ioService, tcp::endpoint(address::from_string(ip), port))
    {
        LOG_INF() << "Initiating server..." << address::from_string(ip) <<":" << port << std::endl;
    }

    int TCPServer::run()
    {
        try
        {
            startAccept();
            m_ioService.run();
        }
        catch (std::exception& e)
        {
            std::cerr << e.what() << std::endl;
            return false;
        }
        return true;
    }

    void TCPServer::broadcast(const std::string &message)
    {
        for (auto& connection : m_connections) {

            connection->post(message);
        }
    }

    void TCPServer::broadcastImage(const std::string& imageName)
    {
        for (auto& connection : m_connections) {

            connection->postImage(imageName);
        }
    }

    void TCPServer::startAccept()
    {
        m_socket.emplace(m_ioService);

        // asynchronously accept the connection
        m_acceptor.async_accept(*m_socket, [this](const boost::system::error_code& error){
            auto connection = Session::Create(std::move(*m_socket));
            if (OnJoin) {
                OnJoin(connection);
            }

            m_connections.insert(connection);
            if (!error) {
                connection->start(
                    [this](const std::string& message) { if (OnClientMessage) OnClientMessage(message); },
                    [this](const std::string& fileName) {if(OnImage) OnImage(fileName);},
                    [&, weak =std::weak_ptr(connection)] {
                        if (auto shared = weak.lock(); shared && m_connections.erase(shared)) {
                            if (OnLeave) OnLeave(shared);
                        }
                    }
                );
            }

            startAccept();
        });
    }


}
