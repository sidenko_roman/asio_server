#include <QCoreApplication>

#include "common.hpp"
#include "tcpServer.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    server::TCPServer server {SERVER_IP, SERVER_PORT};

    server.OnJoin = [](server::Session::pointer session)
    {
        std::cout << "User has joined the server: " << session->getSessionUsername() << std::endl;
    };

    server.OnLeave = [](server::Session::pointer session)
    {
        std::cout << "User has left the server: " << session->getSessionUsername() << std::endl;
    };

    server.OnClientMessage = [&server](const std::string& message)
    {
        std::cout << "OnClientMessage: " << message << std::endl;
        server.broadcast(message);
    };

    server.OnImage = [&server](const std::string& imageName)
    {
        server.broadcastImage(imageName);
    };

    server.run();

    return a.exec();
}
