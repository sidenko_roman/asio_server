#include "common.hpp"
#include "packet.hpp"

Packet::Packet()
    : m_header{MSG_VERSION, MSG_DEFAULT, 0}
    , m_message{""}
{
    ///
}

Packet::Packet(const unsigned int version, const unsigned int type, const unsigned int length, const std::string& message)
    : m_header{version, type, length}
    , m_message{message}
{
    ///
}

unsigned int Packet::getVersion() const {return m_header.m_version;}
unsigned int Packet::getMsgType() const {return m_header.m_type;}
unsigned int Packet::getLength () const {return m_header.m_length;}
unsigned int Packet::getHdrSize() const {return sizeof(m_header);}
unsigned int Packet::getMsgSize() const {return m_message.length();}
unsigned int Packet::getPktSize() const {return getHdrSize() + getMsgSize();}
std::string& Packet::getMessage() {return m_message;}

void Packet::setVersion(const unsigned int version) {m_header.m_version = version;}
void Packet::setType(const unsigned int type) {m_header.m_type = type;}
void Packet::setLength(const unsigned int length) {m_header.m_length = length;}
void Packet::setMessage(const std::string& message) {m_message = message;}

void Packet::set(const unsigned int version, const unsigned int type, const std::string& message)
{
    setMessage(message);
    set(version, type, getPktSize());
}

void Packet::set(const unsigned int version, const unsigned int type,const unsigned int length)
{
    setVersion(version);
    setType(type);
    setLength(length);
}

void Packet::set(const unsigned int version, const unsigned int type, const unsigned int length,const std::string& message )
{
    set(version, type, length);
    setMessage(message);
}
