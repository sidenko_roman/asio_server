#include <exception>
#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/system/error_code.hpp>

#include <QDebug>

#include "common.hpp"
#include "server.hpp"
#include "utilities.hpp"

using boost::bind;
using boost::shared_ptr;
using boost::asio::ip::address;
using boost::system::error_code;
using boost::asio::placeholders::error;

Server::Server(const std::string ip, const unsigned short port, const int threadsCount)
    : m_iosAcceptors{boost::make_shared<io_service>()}
    //, m_wrkAcceptors{boost::make_shared<io_service::work>(*m_iosAcceptors)}
    , m_iosExecutors{boost::make_shared<io_service>()}
    //, m_wrkExecutors{boost::make_shared<io_service::work>(*m_iosExecutors)}
    , m_endpoint{address::from_string(ip), port}
    , m_acceptor{*m_iosAcceptors, m_endpoint}
    , m_newSession{boost::make_shared<Session>(m_iosExecutors)}
    //, m_signals{*m_iosAcceptors, SIGINT, SIGTERM}
{
    /** Add signal handling for graceful termination (CTRL + C) **/
    //m_signals.async_wait(bind(&Server::stop, this));

    /** Normal initialization flow of the server **/

    LOG_INF() << "Initiating server..." << std::endl;

    for(int i = 0; i < threadsCount; ++i)
        m_thgExecutors.create_thread(bind(&Server::WorkerThreadCallback, this, m_iosExecutors));

    m_acceptor.async_accept(m_newSession->getSocket(), bind(&Server::acceptHandler, this, m_newSession, error));

    lockStream();
    LOG_INF() << "Server started! [" << m_endpoint << "]" << std::endl;
    unlockStream();
}

Server::~Server()
{
    stop();

    lockStream();
    LOG_INF() << "Server closed successfully! Bye bye! :)" << std::endl;
    unlockStream();
}

void Server::start()
{
    m_iosAcceptors->run();
}

void Server::stop()
{
    if (!m_iosAcceptors->stopped())
    {
        m_iosAcceptors->stop();
    }

    if (!m_iosExecutors->stopped())
    {
        m_iosExecutors->stop();
        // _thgExecutors.interrupt_all();
        // _thgExecutors.join_all();
    }
}

/** Utility function definitions **/

/* WorkerThread Callback Skeleton */
void Server::WorkerThreadCallback(shared_ptr<io_service> ios)
{
    while(true)
    {
        try
        {
            error_code errorCode;
            ios->run(errorCode);
            if(errorCode)
            {
                lockStream();
                LOG_ERR() << " Error: " << errorCode.message() << std::endl;
                unlockStream();
            }
        }
        catch(const std::exception& ex)
        {
            lockStream();
            LOG_ERR() << " Exception: " << ex.what() << std::endl;
            unlockStream();
        }
    }
}

void Server::acceptHandler(shared_ptr<Session> thisSession, const error_code& ec)
{
    if(!ec)
    {
        LOG_INF() << "Connected to client! ["
                  << getPeerIp(thisSession->getSocket()) << ":"
                  << getPeerPort(thisSession->getSocket()) << "]" << std::endl;
        for(const auto& i : m_connections)
        {
            LOG_INF()<< getPeerName(thisSession->getSocket()) <<std::endl;
            LOG_INF()<< i->getSessionUuid() << std::endl;
        }
        m_iosExecutors->post(bind(&Session::start, thisSession));
        m_connections.insert(thisSession);
        m_newSession = boost::make_shared<Session>(m_iosExecutors);

        m_acceptor.async_accept(m_newSession->getSocket(), bind(&Server::acceptHandler, this, m_newSession, error));
    }
}
