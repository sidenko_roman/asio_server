#include <sstream>
#include <exception>

#include "common.hpp"
#include "session.hpp"
#include "packet.hpp"

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

#include <QRandomGenerator>

namespace server{

using boost::asio::buffer;
namespace fs = boost::filesystem;

Session::Session(tcp::socket &&socket)
    : m_socket{std::move(socket)}
    , m_packet{}
{
    std::stringstream name;
    name << m_socket.remote_endpoint();
    m_username = name.str();

    lockStream();
    LOG_INF() << "Session created for : " << m_username << std::endl;
    unlockStream();
}

void Session::start(MessageHandler&& messageHandler, ImageHandler&& imageHandler, ErrorHandler&& errorHandler)
{
    lockStream();
    LOG_INF() << "Started session for : " << m_username << std::endl;
    unlockStream();

    m_messageHandler = std::move(messageHandler);
    m_errorHandler = std::move(errorHandler);
    m_imageHandler = std::move(imageHandler);

    asyncReadHeader();
}

void Session::stop()
{
    lockStream();
    LOG_INF() << "Session stoped " << std::endl;
    unlockStream();

    m_socket.shutdown(boost::asio::socket_base::shutdown_both);
    m_socket.close();
}

void Session::post(const std::string& message)
{
    m_packet.set(MSG_VERSION, MessageTypes::SendMessageToClient, message);
    asyncWriteHeader();
}

void Session::postImage(const std::string &imageName)
{
    m_packet.set(MSG_VERSION, MessageTypes::SendImageToClient, imageName);
    m_imageName = imageName;
    asyncWriteHeader();
}

void Session::asyncReadHeader()
{
        io::async_read(m_socket, buffer(&m_packet.header(), m_packet.getHdrSize()), io::transfer_exactly(m_packet.getHdrSize()),
                       [this](boost::system::error_code ec, size_t bytesTransferred) {
            if (!ec)
            {
                lockStream();
                LOG_INF() << "Header: "
                            << std::hex << m_packet.header().m_version << ", "
                            << std::hex << m_packet.header().m_type    << ", "
                            << std::dec << m_packet.header().m_length  << std::endl;
                unlockStream();
                switch (m_packet.header().m_type)
                {
                    case MessageTypes::BroadcastMessage:
                {
                    asyncReadMessage();
                    break;
                }

                case MessageTypes::BroadcastImage:
                {
                    asyncReadImage();
                    break;
                }
                default:
                {
                    lockStream();
                    LOG_INF() << "Unsupported packet type " << std::endl;
                    unlockStream();
                    break;
                }
                }
            }
            else
            {
                lockStream();
                LOG_ERR() << "Error: " << ec.message() << std::endl;
                unlockStream();
            }
        });
}

void Session::asyncWriteHeader()
{
    boost::asio::async_write(m_socket, boost::asio::buffer(&m_packet.header(), m_packet.getHdrSize()),
                             [this](boost::system::error_code ec, std::size_t length)
    {
        if (!ec)
        {
            lockStream();
            LOG_INF() << "Header bytes send: " << length << std::endl;
            unlockStream();

            switch (m_packet.header().m_type)
            {
                case MessageTypes::SendMessageToClient:
            {
                lockStream();
                LOG_INF() << "Start do message send work: " << std::endl;
                unlockStream();

                bool queueIdle = m_outgoingMessages.empty();
                std::string sendedStr = m_packet.getMessage();
                sendedStr += "\n"; //added "\n" for use async_read_until
                m_outgoingMessages.push(sendedStr);

                if (queueIdle)
                    asyncWriteMessage();
                break;
            }

            case MessageTypes::SendImageToClient:
            {
                lockStream();
                LOG_INF() << "Start do image work" << std::endl;
                unlockStream();

                m_ifs.open("g:\\TempAsio\\" + m_imageName, std::ios::binary);
                if (!m_ifs.is_open())
                {
                    lockStream();
                    LOG_INF() << "Can't read file" << std::endl;
                    unlockStream();
                    return;
                }
                m_fileSize = fs::file_size("g:\\TempAsio\\" + m_imageName);
                lockStream();
                LOG_INF() << "fileSize: " << m_fileSize << std::endl;
                unlockStream();
                quint32 value = QRandomGenerator::global()->generate();
                std::stringstream ss{ "FileName: " + std::to_string(value)+ m_imageName + "\r\n" + "FileSize: " + std::to_string(m_fileSize) + "\r\n\r\n" };
                ss.getline(m_fileBuf, std::size(m_fileBuf), '*');
                boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, ss.str().size()),
                                         [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteHeader(ec, bytesTransferred);
                                  });

                break;
            }
            default:
            {
                unlockStream();
                LOG_INF() << "Not supported packet type " << std::endl;
                unlockStream();
                break;
            }
            }
        }
        else
        {
           stop();
           return;
        }
    });
}

void Session::asyncReadMessage()
{
    io::async_read_until(m_socket, m_streamBuf, "\n", [this]
    (boost::system::error_code ec, size_t bytesTransferred) {
            onRead(ec, bytesTransferred);
    });
}

void Session::onRead(boost::system::error_code ec, size_t bytesTranferred)
{
    if (ec) {
        m_socket.close(ec);

        m_errorHandler();
        return;
    }

    std::stringstream message;
    message << m_username << ": " << std::istream(&m_streamBuf).rdbuf();
    m_streamBuf.consume(bytesTranferred);
    std::string messageString = message.str().substr(0, message.str().size()-1);
    m_messageHandler(messageString);
    //asyncReadMessage();
    asyncReadHeader();
}

void Session::asyncWriteMessage()
{
    io::async_write(m_socket, io::buffer(m_outgoingMessages.front()), [this]
            (boost::system::error_code ec, size_t bytesTransferred) {
                    onWrite(ec, bytesTransferred);
    });
}

void Session::onWrite(boost::system::error_code ec, size_t bytesTransferred)
{
    if (ec)
    {
        m_socket.close(ec);

        m_errorHandler();
        return;
    }

    m_outgoingMessages.pop();

    if (!m_outgoingMessages.empty())
    {
        asyncWriteMessage();
    }
}

void Session::asyncReadImage()
{
    boost::asio::async_read_until(m_socket, m_sBuf, "\r\n\r\n",
                                  [this](boost::system::error_code const& ec, std::size_t bytesTransferred){
                                    handleReadUntil(ec, bytesTransferred);
                                  });
}

void Session::handleReadUntil(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec && ec != boost::asio::error::eof)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    boost::smatch mr;
    boost::regex regFileName("FileName: *(.+?)\r\n");
    boost::regex regFileSize("FileSize: *([0-9]+?)\r\n");
    std::string headers, tmp;
    while (std::getline(m_istr, tmp) && tmp != "\r")
    {
        headers += (tmp + '\n');
    }
    lockStream();
    LOG_INF() << "Headers:\n" << headers << std::endl;
    unlockStream();
    if (!boost::regex_search(headers, mr, regFileSize))
    {
        unlockStream();
        LOG_INF() << "regFileSize not found\n" << std::endl;
        unlockStream();
        return;
    }
    m_fileSize = std::stoull(mr[1]);
    lockStream();
    LOG_INF() << "fileSize = " << m_fileSize << std::endl;
    unlockStream();
    if (!boost::regex_search(headers, mr, regFileName))
    {
        lockStream();
        LOG_INF() << "regFileName not found" << std::endl;
        unlockStream();
        return;
    }
    std::string fileName = mr[1];
    lockStream();
    LOG_INF() << "fileName = " << fileName << std::endl;
    unlockStream();
    m_imageName = fileName;
    m_ofs.open("g:\\TempAsio\\" + fileName, std::ios::binary);
    if (!m_ofs.is_open())
    {
        lockStream();
        LOG_INF() << "Unable to open output file" << std::endl;
        unlockStream();
        return;
    }
    lockStream();
    LOG_INF() << "sBuf.size() = " << m_sBuf.size() << std::endl;
    unlockStream();

    if (m_sBuf.size())
    {
        m_sizeBody += m_sBuf.size();
        m_ofs << &m_sBuf;
    }
    if (m_sizeBody != m_fileSize)
    {
        boost::asio::async_read(m_socket, m_sBuf, boost::asio::transfer_at_least(1),
                                [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleRead(ec, bytesTransferred);
                                });
    }
    else
    {
        lockStream();
        LOG_INF() << "Transfer complete successfully" << std::endl;
        unlockStream();
    }
}

void Session::handleRead(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec && ec != boost::asio::error::eof)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    m_sizeBody += bytesTransferred;
    m_ofs << &m_sBuf;

    lockStream();
    LOG_INF() << "sizeBody = " << m_sizeBody << std::endl;
    unlockStream();

    if (m_sizeBody != m_fileSize)
    {
        boost::asio::async_read(m_socket, m_sBuf, boost::asio::transfer_at_least(1),
                                [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleRead(ec, bytesTransferred);
                                });
    }
    else
    {
        lockStream();
        LOG_INF() << "Transfer complete successfully" << std::endl;
        unlockStream();
        m_sizeBody = 0;
        m_ofs.close();
        m_imageHandler(m_imageName);
        asyncReadHeader();
    }
}

void Session::handleWriteBody(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }
    lockStream();
    LOG_INF() << "m_sentFileBody : " << m_sentFileBody << " m_fileSize " << m_fileSize << std::endl;
    unlockStream();
    m_sentFileBody += bytesTransferred;
    if (m_ifs)
    {
        m_ifs.read(m_fileBuf, std::size(m_fileBuf));
        boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, m_ifs.gcount()),
                                 [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteBody(ec, bytesTransferred);
                             });
    }
    else
    {
        if (m_sentFileBody != m_fileSize)
        {
            lockStream();
            LOG_INF() << "sentFileBody != fileSize\n" << std::endl;
            unlockStream();
            return;
        }
        lockStream();
        LOG_INF() << "sentFileBody: " << m_sentFileBody << std::endl;
        LOG_INF() << "OK" << std::endl;
        unlockStream();
        m_sentFileBody = 0;
        m_ifs.close();
    }
}

void Session::handleWriteHeader(boost::system::error_code const& ec, std::size_t bytesTransferred)
{
    if (ec)
    {
        lockStream();
        LOG_INF() << ec << ", " << ec.message() << std::endl;
        unlockStream();
        return;
    }

    m_ifs.read(m_fileBuf, std::size(m_fileBuf));
    boost::asio::async_write(m_socket, boost::asio::buffer(m_fileBuf, m_ifs.gcount()),
                             [this](boost::system::error_code const& ec, std::size_t bytesTransferred){handleWriteBody(ec, bytesTransferred);
                         });
}


}//namespace
