#ifndef INCLUDE_PACKET_HPP_
#define INCLUDE_PACKET_HPP_

#include <string>

class Packet
{
    struct Header
    {
        enum
        {
            VERSION_SIZE = 3,
            TYPE_SIZE = 5,
            LENGTH_SIZE = 16
        };

        unsigned int m_version : VERSION_SIZE;
        unsigned int m_type    : TYPE_SIZE;
        unsigned int m_length  : LENGTH_SIZE;
    };

public:
    Packet();

    Packet(const unsigned int version, const unsigned int type, const unsigned int length, const std::string& message);

    Header& header(){return m_header;}
    std::string& getMessage();

    unsigned int getVersion() const;
    unsigned int getMsgType() const;
    unsigned int getLength() const;

    unsigned int getHdrSize() const;
    unsigned int getMsgSize() const;
    unsigned int getPktSize() const;

    void setVersion(const unsigned int version);
    void setType(const unsigned int type);
    void setLength(const unsigned int length);
    void setMessage(const std::string& message);

    void set(const unsigned int version, const unsigned int type, const std::string& message);
    void set(const unsigned int version, const unsigned int type, const unsigned int length);
    void set(const unsigned int version, const unsigned int type, const unsigned int length, const std::string& message);

private:
    Header m_header;
    std::string m_message;
};

#endif /* INCLUDE_PACKET_HPP_ */
