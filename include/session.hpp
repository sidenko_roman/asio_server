#ifndef INCLUDE_SESSION_HPP_
#define INCLUDE_SESSION_HPP_

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <fstream>

#include "packet.hpp"
#include <queue>

using boost::asio::ip::tcp;
using boost::asio::io_service;
using boost::shared_ptr;


namespace server {

    namespace io = boost::asio;

    using MessageHandler = std::function<void(std::string)>;
    using ErrorHandler = std::function<void()>;
    using ImageHandler = std::function<void(std::string)>;

class Session
{
public:
    using pointer = std::shared_ptr<Session>;

    static pointer Create(io::ip::tcp::socket&& socket) {
        return pointer(new Session(std::move(socket)));
    }

    void start(MessageHandler&& messageHandler, ImageHandler &&imageHandler, ErrorHandler &&errorHandler);
    void stop();

    void post(const std::string &message);
    void postImage(const std::string &imageName);

    std::string& getSessionUsername(){return m_username;}

private:
    explicit Session(io::ip::tcp::socket&& socket);

    void asyncReadHeader();
    void asyncWriteHeader();

    void asyncReadMessage();
    void onRead(boost::system::error_code ec, size_t bytesTranferred);

    void asyncWriteMessage();
    void onWrite(boost::system::error_code ec, size_t bytesTransferred);

    void asyncReadImage();
    void handleReadUntil(boost::system::error_code const& ec, std::size_t bytesTransferred);
    void handleRead(boost::system::error_code const& ec, std::size_t bytesTransferred);

    //for image processing
    void handleWriteBody(boost::system::error_code const& ec, std::size_t bytesTransferred);
    void handleWriteHeader(boost::system::error_code const& ec, std::size_t bytesTransferred);

    tcp::socket m_socket;
    Packet m_packet;
    std::string m_username;

    io::streambuf m_streamBuf {65536};
    std::queue<std::string> m_outgoingMessages;

    MessageHandler m_messageHandler;
    ErrorHandler m_errorHandler;
    ImageHandler m_imageHandler;

    //for images
    io::streambuf m_sBuf;
    std::istream m_istr{&m_sBuf};
    std::size_t m_fileSize;
    std::size_t m_sizeBody{0};
    std::ofstream m_ofs;
    char m_buf[1024];

    //for send image
    std::ifstream m_ifs;
    std::size_t m_sentFileBody{0};
    char m_fileBuf[4096];
    std::string m_imageName;

    enum MessageTypes
    {
        BroadcastMessage,
        BroadcastImage,
        SendMessageToClient,
        SendImageToClient
    };
};
}

#endif /* INCLUDE_SESSION_HPP_ */
