#ifndef INCLUDE_SERVER_HPP_
#define INCLUDE_SERVER_HPP_

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/system/error_code.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <set>

#include "session.hpp"
#include "common.hpp"

using boost::shared_ptr;
using boost::asio::ip::tcp;
using boost::asio::io_service;
using boost::system::error_code;
using boost::enable_shared_from_this;

class Server
{
public:
    Server(const std::string ip, const unsigned short port, const int threadsCount);

    ~Server();

    void start();
    void stop ();

private:
    void WorkerThreadCallback(shared_ptr<io_service> ios);

    void acceptHandler(shared_ptr<Session> thisSession, const error_code& ec);
    //void acceptNewConn();

    shared_ptr<io_service>          m_iosAcceptors;
    //shared_ptr<io_service::work>    m_wrkAcceptors;

    shared_ptr<io_service>          m_iosExecutors;
    //shared_ptr<io_service::work>    m_wrkExecutors;

    boost::thread_group             m_thgExecutors;

    tcp::endpoint                   m_endpoint;
    tcp::acceptor                   m_acceptor;

    shared_ptr<Session>             m_newSession;
    std::set<Session::pointer> m_connections {};

    //boost::asio::signal_set         m_signals;
};

#endif /* INCLUDE_SERVER_HPP_ */
