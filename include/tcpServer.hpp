#ifndef INCLUDE_TCP_SERVER_HPP_
#define INCLUDE_TCP_SERVER_HPP_

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <functional>
#include <optional>
#include <unordered_set>

#include "session.hpp"
#include "common.hpp"

using boost::shared_ptr;

namespace server {
    namespace io = boost::asio;

    class TCPServer {
        using OnJoinHandler = std::function<void(Session::pointer)>;
        using OnLeaveHandler = std::function<void(Session::pointer)>;
        using OnClientMessageHandler = std::function<void(std::string)>;
        using OnImageHandler = std::function<void(std::string)>;

    public:
        TCPServer(const std::string& ip, int port);

        int run();
        void broadcast(const std::string& message);
        void broadcastImage(const std::string& imageName);
    private:
        void startAccept();

    public:
        OnJoinHandler OnJoin;
        OnLeaveHandler OnLeave;
        OnClientMessageHandler OnClientMessage;
        OnImageHandler OnImage;

    private:
        io::io_context m_ioService;
        io::ip::tcp::acceptor m_acceptor;

        std::optional<io::ip::tcp::socket> m_socket;
        std::unordered_set<Session::pointer> m_connections {};
    };
}

#endif /* INCLUDE_TCP_SERVER_HPP_*/

